# devPlan API documentation

This is documentation of devPlan API also named as Cash. Docs are build on top of the [Jekyll](http://jekyllrb.com/).

Documentation is available **[on-line](http://devplan.uek.krakow.pl/docs/)**

## Usage

```
bundle install # install required gems
jekyll serve # now you can access the documentation on http://localhost:4000
```