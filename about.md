---
layout: page
title: About
permalink: /about/
---
#### Timetables of the next generation

devPlan is summer project of [DEV](http://dev.uek.krakow.pl/) group. But what is UEK and DEV? UEK stands for [Cracow University of Economics](http://uek.krakow.pl/) and DEV is group of local developing enthusiast that meets every week on UEK campus.

This summer we decided to help our "great" University and create alternative timetable service for all students and staff. Field of possible improvements is very wide. Our goal is to push timetables to the next level and make them maximally useful. 

## How does it work?

Key ingredient of UEK Timetable is "[Tango](https://bitbucket.org/knp_team/uek-tango) and [Cash](https://bitbucket.org/knp_team/uek-cash)". Although two of them have very different functions, together they form perfect team. To see how UEK Timetable work in detail, look on above diagram.

[![How devPlan works](https://bitbucket.org/Mckomo/devplan-docs/raw/HEAD/assets/devplan-diagram.png)](https://bitbucket.org/Mckomo/devplan-docs/raw/HEAD/assets/devplan-diagram.png)

As you can see, diagram has marked number. They refer to each step of UEK Timetable service. Let's describe them.

1. **Tango** crawls through current timetable website and downloads pages that contain data about events on the University. 
2. Now received website source ( XML ) is being scrapped to extract required data about all events.
3. **Tango** will now transform the data and load it to the data base system. Note that data base system actuality consist of two slave dbs. First db for write and second for read purposes. After full cycle of **Tango**, dbs are being switched. This approach will speed up the whole service.
4. Handling timetable request is **Cash** job. Through it's RESTful API, **Cash** is great gateway to UEK Timetable resources.   
5. Desired data comes from the data base system. **Cash** is ready to respond. Default data format is JSON.

Hopefully, now you got right picture of devPlan :)
