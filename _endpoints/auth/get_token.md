---
layout: endpoint
request_path: "/auth/access_token"
resource: access_token
method: "GET"
fields:
  -
    name: login
    required: true
    type: string
    description: The user login
  -
    name: password
    required: true
    type: string
    description: Password encoded to the Base64
request_examples:
  -
    url: "/auth/access_token"
    parameters: 
      login: mckomo
      password: "c2VjcmV0X3Bhc3N3b3JkCg=="
    response:
      access_token: ba053a45ba7770acd570df65fb72065e7e66828e261a1d41d8ee3db8
      expires_at: 2014-11-22T17:15:18.468+01:00
---

Obtaining the access token for protected endpoints of the API.