---
layout: endpoint
resource: place
request_path: "/places"
method: GET
parameters:
---

### Parameters
None

### Example request:

**GET** http://devplan.uek.krakow.pl/api/places

```
[
   {  
      "id":1,
      "name":"Paw. C aula nowa",
      "full_name":"Pawilon C aula nowa",
      "regular":true
   },
   ...
   {  
      "id":98,
      "name":"Paw. F s. 717",
      "full_name":"Pawilon F sala 717",
      "regular":true
   }
]
```