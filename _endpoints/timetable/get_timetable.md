---
layout: endpoint
resource: timetable
request_path: "/timetables/:id"
method: "GET"
---

Returns previously registered timetable containing **activities**.

### Parameters



| Name      | Type              | Description
|:---------:|:-----------------:| ---
| **id**    | String *required* | Id
| **scope** | String *optional* | Scope
 

### Example requests:

**GET** http://devplan.uek.krakow.pl/api/timetables/g552g3328

```
{  
   "_id":"g552g3328",      
   "params":{              
      "group_id":[  
         552,
         3328
      ]
   },
   "version":"5096146ca522d316c87b217b83a6c4d2",
   "activities":[ 
      { 
         "id":18282,
         "places":[           
            {  
               "id":97,                          
               "name":"Paw. C s. A",             
               "full_name":"Pawilon C sala A",   
               "regular":true                    
            }
         ],
         "tutors":[
            {  
               "id":266,
               "name":"prof. UEK Paweł Lula", 
               "short_name":"P. Lula",
               "moodle_url":"https://e-uczelnia.uek.krakow.pl/course/view.php?id=1034",
               "regular":true
            }
         ],
         "groups":[  
            {  
               "id":552,
               "name":"KrDZIs3011Io",
               "type":1,
               "type_name":"Grupa dziekańska"
            },
            {  
               "id":553,
               "name":"KrDZIs3011Si",
               "type":1,
               "type_name":"Grupa dziekańska"
            },
            {  
               "id":554,
               "name":"KrDZIs3012Si",
               "type":1,
               "type_name":"Grupa dziekańska"
            }
         ],
      "category":1               
      "category_name":"wykład",
      "name":"Teoria grafów",   
      "regular_course":true,
      "notes":"",
      "url":"",
      "date":"2014-02-07",
      "day_of_week":5,
      "day_of_week_text":"Pt",
      "starts_at":"9:35",
      "ends_at":"14:45",
      "regular_schedule":true,
      "starts_at_timestamp":1391765400,
      "ends_at_timestamp":1391771100, 
      "unit_ordinal_number":"1",
      "unit_total_count":"15", 
      "canceled":false,
      "canceled_reason":""
         },
      ...
   ]
}
```

**GET** http://devplan.uek.krakow.pl/api/timetables/g552g3328/version

```
{  
   "_id":"g552g3328",
   "version":"16377fc41997afd73423f43250530b22"
}
```