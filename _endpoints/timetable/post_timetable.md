---
layout: endpoint
resource: timetable
request_path: "/timetables"
method: "POST"
parameters:
 
examples:
  - <<
    {  
      "_id":"g552g3328",
      "access_url":"http://devplan.uek.krakow.pl/api/timetables/g552g3328",
      "params":{  
        "group_id":[  
           552,
           3328
        ]
      }
    }
  - "TEST"
---

Returns previously registered timetable containing **activities**.

##### Resource URI:

```
POST /timetables
```
##### Parameters

| Name         | Type                       | Description |
|:------------:|:--------------------------:| ----------- |
| **group_id** | String or Array *optional* | Id or ids of groups
| **place_id** | String or Array *optional* | Id or ids of places
| **tutor_id** | String or Array *optional* | Id or ids of tutors

You have to pass at least one parameter.

##### Example request:

**POST** http://devplan.uek.krakow.pl/api/timetables
``` "group_id[]=552&group_id[]=3328"```

```
{  
   "_id":"g552g3328",
   "access_url":"http://devplan.uek.krakow.pl/api/timetables/g552g3328",
   "params":{  
      "group_id":[  
         552,
         3328
      ]
   }
}
```