---
layout: endpoint
resource: tutor
request_path: "/tutors"
method: GET
parameters:
---

### Parameters
None

### Example request:

**GET** http://devplan.uek.krakow.pl/api/tutors

```
[
   {  
      "id":1,
      "name":"dr Marcin Kędzior",
      "short_name": "M. Kędzior",
      "moodle_url":"https://e-uczelnia.uek.krakow.pl/course/view.php?id=651",
      "regular":true
   },
   ...
   {  
      "id":156,
      "name":"dr Zbigniew Stańczyk",
      "short_name": "Z. Stańczyk",
      "moodle_url":"https://e-uczelnia.uek.krakow.pl/course/view.php?id=367",     
      "regular":true
   }
]
```