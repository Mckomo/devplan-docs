---
layout: endpoint
resource: group
request_path: "/groups"
method: GET
request_examples:
  -
    url: /groups
    response:
      -
        id: 507f191e810c19729de860ea,
        name: KrDZIs3011Io,
        type: 1,
        type_name: Grupa dziekańska
        version: 70022fa172a4be8daab221d3e713c97c
      - '...'     
---

Returns an array with all [groups]({{ site.baseurl }}/resources/group) without their activities.