---
layout: endpoint
resource: group
request_path: "/groups/:id"
method: GET
parameters:
  -
    name: id
    required: true
    type: string
    description: The group ID
request_examples:
  -
    url: /groups/507f191e810c19729de860ea
    response:
      id: 507f191e810c19729de860ea,
      name: KrDZIs3011Io,
      type: 1,
      type_name: Grupa dziekańska
      activities:
        - '...'      
---

Returns the [group]({{ site.baseurl }}/resources/group) with the given ID.