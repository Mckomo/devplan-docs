---
layout: endpoint
resource: group
request_path: "/groups"
method: POST
parameters:
  -
    name: name
    required: true
    type: string
    description: Name of the new group
  -
    name: type
    type: string
    description: Type of the new group. <a href="../../../resources/group#enum-type">More details</a>
  - 
    name: access_token
    type: string
    description: Valid <a href="../../auth/get_token">access token</a>
permissions:
  - Valid access token is required
request_examples:
  -
    parameters:
      name: Koła Naukowe >DEV
    response:
      id: 217f191e810c19729de860ea,
      name: Koła Naukowe >DEV,
      type: 0,
      type_name: Grupa specjalna
      version: 4dd4ee568d91bb545d8996ee728046cd
      activities: []    
---

Creates new [group]({{ site.baseurl }}/resources/group). Should return `201` HTTP code when successful.