---
layout: page
title: "Getting started"
permalink: "/start/"
---

### Example usage - registering your own timetable

Let's say you want to create your own timetable. You can achieve that in a few simple steps. To make an example lets establish that you belong to two groups: *KrDZIs3011Io* (6th semester of applied computer scinece) and *SJO-DUGaA025B2* (B2 level of the English course).

**1.** Get list of all groups to find ids of yours. 

**Request**:

```
GET http://devplan.uek.krakow.pl/api/groups   
```

**Response:**
  
```
[
  ...
   {  
      "id":552,
      "name":"KrDZIs3011Io",
      "type":1,
      "type_name":"Grupa dziekańska"
   },
   {  
      "id":553,
      "name":"KrDZIs3011Si",
      "type":1,
      "type_name":"Grupa dziekańska"
   },
   {  
      "id":554,
      "name":"KrDZIs3012Si",
      "type":1,
      "type_name":"Grupa dziekańska"
}
  ... 
]
```
  
**2.** Now you can see that your groups have ids 12 and 51. With that knowledge you can register your own timetable.
  
**Request**:

```
POST http://devplan.uek.krakow.pl/api/timetables "group_id[]=552&group_id[]=1403"
```
  
**Response:**
  
```
{  
   "_id":"g552g1403",
   "access_url":"http://devplan.uek.krakow.pl/api/timetables/g552g1403",
   "params":{  
      "group_id":[  
         552,
         1403
      ]
   }
}
```

**3a.** Now you can get your freshly created timetable.

**Request**:

```
GET http://devplan.uek.krakow.pl/api/timetables/g552g1403
```
  
**Response:**

```
{  
   "_id":"g552g1403",
   "params":{  
      "group_id":[  
         552,
         1403
      ]
   },
   "version":"a044b9341e0c322b8fbec2db4a2cd8c2",
   "activities":[
    ...
   ]
}
```

**3b.** Moreover, just to look up whether your timetable has changed, you can download lighter `version` scope of timetable.

**Request**

```
http://devplan.uek.krakow.pl/api/timetables/g552g1403/version
```

**Response:**

```
{  
   "_id":"g552g1403",
   "version":"a044b9341e0c322b8fbec2db4a2cd8c2"
}
```