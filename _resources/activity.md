---
layout: resource
name: "Activity"
fields:
  -
    name: id
    type: Integer
    specification:
    description: "Unique identifier, but not persistent. Can change with a timetable update"
  -
    name: groups
    type: Array
    specification: 
    description: "Array of zero, one or many groups that participate in the activity"
  -
    name: places
    type: Array
    specification: 
    description: "Array of zero, one or many places where the activity take place"
  -
    name: tutors
    type: Array
    specification: 
    description: "Array of zero, one or many tutors that conduct the activity"
  -
    name: name
    type: String
    specification: 
    description: "Name of the activity"
  -
    name: category
    type: Integer
    specification: 
    description: "Number of the category"
  -
    name: category_name
    type: String
    specification: 
    description: "Name of the category from the <code>category</code> field"
  -
    name: regular_course
    type: Boolean
    specification: 
    description: "<em>True</em> if the activity is a part of a regular university course, otherwise <em>false</em>"
  -
    name: notes
    type: String
    specification: 
    description: "Notes attached to the activity, can be <strong>empty string</strong>"
  -
    name: url
    type: String
    description: "URL attached to the activity, can be <strong>empty string</strong>"
    specification: 
  -
    name: date
    type: String
    specification: 
    description: "Date of the activity in <code>yyyy-mm-dd<prem> format"
  -
    name: day_of_week
    type: Integer
    specification: 
    description: "Number of the day of the week from the <code>date</code> field. Can be an integer from [0, 6] range, where Sunday is 0"
  -
    name: day_of_week_text
    type: String
    description: "Name of the day of a week from the <code>day_of_week</code> field"
    specification: 
  -
    name: starts_at
    type: String
    description: "Time of the activity beginning in <code>H:mm</code> format"
    specification:
  -
    name: ends_at
    type: String
    specification:
    description: "Time of the activity ending in <code>H:mm</code> format"
  -
    name: starts_at_timestamp
    type: Integer
    description: "Timestamp of the activity beginning"
    specification:
  -
    name: ends_at_timestamp
    type: Integer
    description: "Timestamp of the activity ending"
    specification:
  -
    name: regular_schedule
    type: Boolean
    description: "<em>true</em> if the activity start and end time match university regular timetable, otherwise <em>false</em>"
    specification: 
  -
    name: unit_ordinal_number
    type: Integer
    description: "Ordinal number of the activity in a course block"
    specification:
  -
    name: unit_total_count
    type: Integer
    description: "Total number of activities in a course block"
    specification: 
  -
    name: canceled
    type: Boolean
    description: "<em>True</em> if the activity has been canceled, otherwise <em>false</em>"
    specification:
  -
    name: canceled_reason
    type: String
    description: "Reason why the activity was canceled, can be <strong>empty string</strong>"
    specification:
enums:
  -
    title: "Categories of an activity"
    labels:
      key: "Category"
      value: "Category name"
    elements:
      -
        key: 0
        value: "specjalne"
        default: true
      -
        key: 1
        value: "wykład"
      -
        key: 2
        value: "ćwiczenia"
      - 
        key: 3
        value: "lektorat"
      - 
        key: 4
        value: "egzamin"

examples:
  -
    places: []
    tutors: []
    groups: []
    category: 1
    category_name: wykład
    name: Teoria grafów
    regular_course: true
    notes: ''
    url: ''
    date: '2014-02-07'
    day_of_week: 5
    day_of_week_text: Piątek
    starts_at: '9:35'
    ends_at: '14:45'
    regular_schedule: true
    starts_at_timestamp: 1391765400
    ends_at_timestamp: 1391771100
    unit_ordinal_number: '1'
    unit_total_count: '15'
    canceled: false
    canceled_reason: ''
---