---
layout: resource
name: "Place"
fields:
  -
    name: _id
    type: string
    description: "Unique persistent identifier"
  -
    name: name
    type: string
    description: "Full name of the place without any abbreviation"
  -
    name: short_name
    type: string
    description: "Default abbreviated form of the place's name"
  - 
    name: regular
    type: Boolean
    specification: "<em>false</em> be default"
    description: "<em>true</em> if the place is a regular university classroom, otherwise <em>false</em>."
examples: 
  -
    id: 507f191e810c19729de860ea
    name: "Pawilon C sala A"
    short_name: "Paw. C s. A"
    regular: true
---