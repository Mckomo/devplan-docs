---
layout: resource
name: "Timetable"
fields:
  -
    name: _id
    type: string
    description: "Unique persistent identifier"
  -
    name: version
    type: string
    description: "Version of the timetable. It changes whenever one or more <code>activities</code> have changed"
  -
    name: components
    type: Dictionary
    description: Dictionary with arrays of groups, places and/or tutors specified by parameters used in POST request (see example)
  - 
    name: activities
    type: Array
    description: "Array of zero, one or many activities"
examples: 
  -
    id: "g552g3328"
    version: "Pawilon C sala A"
    components:
      groups:
         -
          _id: 507f1f77bcf86cd799439011
          name: "KrDZIs3011Io"
          type: 1
          type_name: "Grupa dziekańska"
      places:
         -
            id: 507f191e810c19729de860ea
            name: "Pawilon C sala A"
            short_name: "Paw. C s. A"
            regular: true

    activities:
      - "// activity objects"
---