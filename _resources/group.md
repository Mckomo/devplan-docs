---
layout: resource
name: "Group"
fields:
  -
    name: _id
    type: string
    description: "Unique persistent identifier"
  -
    name: name
    type: string
    description: "Name of the group"
  -
    name: type
    type: string
    description: "Type of the group as integer"
  -
    name: type_name
    type: string
    description: "Name of the type from the <code>type</code> field"
  -
    name: version
    type: string
    description: 'MD5 hash of the group object (including activities)'
  - 
    name: activities
    type: Array
    description: List of the group's <a href="{{ site.baseurl }}/resources/activity">activities</a>
enums:
  -
    field: "type"
    title: "Types of a group"
    labels:
      key: Type
      value: Type name
    elements:
      -
       key: 0
       value: "Grupa specjalna"
      -
       key: 1
       value: "Grupa dziekańska" 
      -
       key: 2
       value: "Grupa językowa" 
      -
       key: 3
       value: "Grupa seminaryjna" 
examples:
  - 
    id: 507f191e810c19729de860ea,
    name: KrDZIs3011Io,
    type: 1,
    type_name: Grupa dziekańska
    version: 70022fa172a4be8daab221d3e713c97c
    activities:
      - '...'
---