---
layout: resource
name: "Tutor"
fields:
  -
    name: id
    type: Integer
    description: "Unique persistent identifier"
  -
    name: name
    type: string
    description: "Full name of a tutor with titles"
  -
    name: short_name
    type: string
    description: "Name shortcut without titles and other short forms"
  -
    name: profile_url
    type: string
    description: "URL address to tutor's profile, can be <strong>empty string</strong>"
  - 
    name: regular
    type: Boolean
    specification: "<em>false</em> be default"
    description: "<em>true</em> if tutor is regular university employee, otherwise <em>false</em>"
examples: 
  -
    id: 266
    name: "prof. UEK Paweł\x82 Lula"
    short_name: P. Lula
    profile_url: https://e-uczelnia.uek.krakow.pl/course/view.php?id=1034
    moodle_url: https://e-uczelnia.uek.krakow.pl/course/view.php?id=1034
    regular: true
---